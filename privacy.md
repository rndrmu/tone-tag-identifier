

Tone Tag Identifier evaluates, but not saves message content
Furthermore user name + tag pair, command name + time of invoke, are logged to focus dev time on the right things

Tone Tag Identifier needs this data to function correctly

    Message content to scan for tone tags in a message

If you have any concerns, please contact the development team via the support server (https://discord.gg/U7Dz3n94ev) or contact rndrmu#0001 directly for data removal or security concerns.
