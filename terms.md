Terms of Service

Effective: January 08 2023.
Last updated: January 08 2023.

1. Introduction

Thanks for being interested in using Tone Tag Identifier. Before doing so, you should carefully read, understand and accept these Terms of Service and the corresponding Privacy Policy (accessible at https://gitlab.com/rndrmu/tone-tag-identifier/-/blob/main/privacy.md, collectively referred to as "Terms").

This is a legal agreement rndrmu ("I", "My", "rndrmu", "Tone Tag Identifier") and you ("Your"). By using this Discord bot (collectively referred to as "Tone Tag Identifier", "Bot" or "Service"), you are agreeing to the following Terms of Service. Please read them carefully.

In cases of doubt, the owner of the server on which Tone Tag Identifier is is held liable and is referred to as "You" in these Terms.

To fully understand and being able to agree to these Terms, you will have to read, understand and accept Discord Inc. ("Discord")'s Terms of Service (accessible at https://discord.com/terms), first.

Tone Tag Identifier reserves the right to update these Terms, which may be done for reasons that include, but are not limited to, complying with changes to the law or reflecting enhancements to Tone Tag Identifier. If the changes affect your usage of Tone Tag Identifier or your legal rights, a message is sent into the "#updates" channel (ID: 1026589024913203201) of Tone Tag Identifier's Discord Support Server (ID: 740492918367846481, accessible at https://discord.gg/U7Dz3n94ev). You will be notified no less than 14 days before the changes take effect. Unless I state otherwise, your continued use of Tone Tag Identifier after the modifications are posted will constitute your acceptance of and agreement to those changes. You agree that you won't be notified on changes to these Terms when not being part of the support server or disabled notifications of this server. You agree that you may be excluded from the support server when not following the rules stated in the "#rules" channel (ID: 849720880095297576) and then won't be provided with any updates on any changes to these Terms. Either way, it is highly recommended to periodically check on these Terms to ensure that you still agree to them.


2. About this service

Tone Tag Identifier is a bot for the chat and social platform "Discord" (accessible at https://discord.com), provided by Discord Inc. This service allows you to make use of Discord's public API (its documentation is accessible at https://discord.dev/, including its Terms of Service and Privacy Policy).

Tone Tag Identifier is strictly following Discord's "Developer terms", does not want, is not trying to and is not knowingly abusing any of Discord's services. YOU ARE NOT ALLOWED TO USE THIS SERVICE OR ITS PROVIDED FEATURES OTHER THAN INTENDED. YOU MAY NOT USE Tone Tag Identifier TO SEND DIRECT MESSAGE ADVERTISEMENTS TO USERS, SEND MULTIPLE MESSAGES IN A SHORT TIME SPAN TO USERS OR IN CHANNELS OR ABUSE Tone Tag Identifier IN ANY OTHER WAY THAT COULD BE AGAINST THIS OR DISCORD'S TERMS. If you find out that someone is abusing Tone Tag Identifier's features, please let me know as soon as possible.


3. License

You may use Tone Tag Identifier and its services for any purpose, including commercial use, if not stated otherwise in this Terms. You may not reverse-engineer or otherwise attempt to derive source code from Tone Tag Identifier.


4. Restrictions on your use of Tone Tag Identifier's services

You may not do the following:

    please don't spam the bug report form

5. Support and bugs

I am more than happy to answer you questions and fix bugs regarding Tone Tag Identifier. If you need help or found a bug you can visit the channel #support on Tone Tag Identifier's Discord server.


6. Outages, termination and general service

Tone Tag Identifier and its services are completely free. I am actively developing new features and fix bugs to improve Tone Tag Identifier and your user experience. Therefore I may add or remove features. While I try to avoid disruptions, I cannot guarantee that there will not be an outage. I am not liable for any such outages or service changes, especially because Tone Tag Identifier is a free service, apart from Custom Branding.

You are free to stop using Tone Tag Identifier's services at any time for any reason by kicking Tone Tag Identifier from your Discord server.

